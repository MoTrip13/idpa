package ch.mbcoding.controller;

import ch.mbcoding.main.Main;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;


/**
 * @author Moritz Bolliger
 * @version 26.09.2019 10:55
 * @project Package ch.mbcoding.controller von Projekt Pathfinder
 */
public class MainGuiController {

    @FXML
    private GridPane grid;
    @FXML
    private StackPane stackPane;
    @FXML
    public Button btnRun, btnCancel, btnSetsize, btnSetstart, btnSetend, btn4, btnSetvalue, btnRandomvalue, btnSizeSubmit, btnSizeCancel;
    @FXML
    private TextArea algorithmDes;
    @FXML
    private TextField txtFieldWidth;
    @FXML
    private TextField txtFieldHeight;
    @FXML
    private ComboBox algorithmSelect;
    @FXML
    private Label lbMap;

    private Label labelOldStart = null, labelOldEnd = null;

    private int oldWidth, oldHeight;

    Boolean settableStart = false, settableEnd = false;


    public MainGuiController() {
    }

    @FXML
    public void initialize(){

        //Sets the stylesettings for the Gui at the beginning
        setupStyle();

        //Sets the values for the combobox
        setupCombobox();

        //If the red button "Cancel" is clicked, the Gui gets closed
        btnCancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage primaryStage = (Stage) btnCancel.getScene().getWindow();
                Main.close(primaryStage);
            }
        });

        //If the green Button "Run" ist clicked, the Algorithm gets started.
        btnRun.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //TODO
            }
        });

        btnSetsize.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //Let the controls appear on the gui
                txtFieldHeight.setVisible(true);
                txtFieldWidth.setVisible(true);
                btnSizeSubmit.setVisible(true);
                btnSizeCancel.setVisible(true);

                // When submit is clicked
                btnSizeSubmit.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        resizeGridpane();
                    }
                });

                // When Cancel is clicked, all the fields and buttons to set the gridSize, disappear.
                btnSizeCancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        txtFieldWidth.setVisible(false);
                        txtFieldHeight.setVisible(false);
                        btnSizeSubmit.setVisible(false);
                        btnSizeCancel.setVisible(false);

                        //The lable will display No Map Again if no or wrong size is entered
                        lbMap.setText("No Map");
                        lbMap.setStyle("-fx-text-fill: black; -fx-font-size: 20px");
                    }
                });
            }
        });

        algorithmSelect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                String algorithm = algorithmSelect.valueProperty().getValue().toString();

                switch (algorithm){

                    case "Dijkstra" :
                        algorithmDes.setText("Description for Dijkstra");
                        break;

                    case "A*-Algorithm" :
                        algorithmDes.setText("Description for A*-Algorithm");
                        break;

                    case "Bellman Ford" :
                        algorithmDes.setText("Description for Bellman Ford");
                        break;

                    case "Greedy" :
                        algorithmDes.setText("Description for Greedy");
                        break;
                }
            }
        });

    }

    //Sets the values for the combobox
    private void setupCombobox() {
        algorithmSelect.setItems(FXCollections.observableArrayList(
                "Dijkstra",
                "A*-Algorithm",
                "Bellman Ford",
                "Greedy"
        ));
    }

    //The method is used to set the initial styles
    public void setupStyle(){
        // Set Colors for lower Buttons
        btnRun.setStyle("-fx-background-color: green");
        btnCancel.setStyle("-fx-background-color: red");

        //Show the lines between the cells gridPane
        grid.setGridLinesVisible(true);

        //Sets the Fields and Buttons invisible
        txtFieldHeight.setVisible(false);
        txtFieldWidth.setVisible(false);
        btnSizeSubmit.setVisible(false);
        btnSizeCancel.setVisible(false);
    }

    //If you want to resize the field (gridpane) this method ist used to do so
    public void resizeGridpane(){
        //Check if the Values in the Textfields are only numbers between 0 and 9 and that min 1 number is inserted.
        if (txtFieldHeight.getText().matches("[0-9]{1,}") && txtFieldWidth.getText().matches("[0-9]{1,}")) {
            //Checks if the values are below 80 for the width and below 54 for the height, so it can be displayed like a grid
            if (Integer.parseInt(txtFieldWidth.getText()) <= 80 && Integer.parseInt(txtFieldHeight.getText()) <= 54) {

                //Get the Values from the Textfields and safe them as an Integer
                int fieldWidth = Integer.parseInt(txtFieldWidth.getText());
                int fieldHeight = Integer.parseInt(txtFieldHeight.getText());

                //Calls the method to make the new field with the new size
                lbMap.setVisible(false);
                setupGridpane(fieldHeight, fieldWidth);

                //Remove the controls again from the gui
                txtFieldWidth.setVisible(false);
                txtFieldHeight.setVisible(false);
                btnSizeSubmit.setVisible(false);
                btnSizeCancel.setVisible(false);
            } else {
                //Removes Grid so errormessage can be displayed
                removeGrid();

                //Errormessage for the wrong fieldsize will be displayed
                lbMap.setText("Max width value is 80 and max height value is 54");
                lbMap.setStyle("-fx-text-fill: red; -fx-font-size: 14px");
                lbMap.setVisible(true);
            }
        } else {
            //If the Input are not a numbers between 0 and 9, the textfield gets wiped and you have to put in a new value.
            txtFieldWidth.setText("");
            txtFieldHeight.setText("");
        }
    }

    public void removeGrid(){
        // Deletes the rows and the columns, so the new field with the new size can be built from ground up
        grid.getColumnConstraints().remove(0, oldWidth);
        grid.getRowConstraints().remove(0, oldHeight);
    }

    // Sets up the field (gridpane)
    public void setupGridpane(int fieldHeight, int fieldWidth){

        String text = "";

        removeGrid();

        // Keeps the old size values of the field, that we can delete the "old" number of rows and columns
        //Here the old values get set with the new values
        oldHeight = fieldHeight;
        oldWidth = fieldWidth;


        //Builds the rows and columns of the field
        for (int i = 0; i < fieldWidth; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setMaxWidth(stackPane.getWidth() / fieldWidth);
            colConst.setMinWidth(stackPane.getWidth() / fieldWidth);
            grid.getColumnConstraints().add(colConst);
        }

        for (int i = 0; i < fieldHeight; i++){
            RowConstraints rowConst = new RowConstraints();
            rowConst.setMaxHeight(stackPane.getHeight() / fieldHeight);
            rowConst.setMinHeight(stackPane.getHeight() / fieldHeight);
            grid.getRowConstraints().add(rowConst);
        }

        fillGridpane(fieldHeight, fieldWidth);
    }

    public void fillGridpane(int fieldHeight, int fieldWidth){

        for (int i = 0; i < fieldWidth; i++) {
            for (int j = 0; j < fieldHeight; j++){

                Label label = new Label("test");

                final int tempI = i;
                final int tempJ = j;


                        //Marks the selected label and sets it as start
                        label.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                //If the button SetStart is selected
                                if (settableStart) {
                                    //The selected Startfield is marked  green
                                    label.setStyle("-fx-background-color: green");

                                    //If there is an old Startfield, it is unmarked
                                    if (labelOldStart != null) {
                                        labelOldStart.setStyle("-fx-background-color: clear");
                                    }

                                    settableStart = false;

                                    //The now selected label gets copied to the temporary labelOld for Start
                                    labelOldStart = label;
                                }

                                //If the button SetEnd is selected
                                if (settableEnd){
                                    //The selected Endfield is marked  red
                                    label.setStyle("-fx-background-color: red");

                                    //If there is an old Endfield, it is unmarked
                                    if (labelOldEnd != null) {
                                        labelOldEnd.setStyle("-fx-background-color: clear");
                                    }

                                    settableEnd = false;

                                    //The now selected label gets copied to the temporary labelOld for End
                                    labelOldEnd = label;
                                }

                            }
                        });

                //If the button setStart is clicked the user can click the field where the Start should be
                btnSetstart.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (!settableStart){
                            settableStart = true;
                        }
                    }
                });

                //If the button setStart is clicked the user can click the field where the Start should be
                btnSetend.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (!settableEnd){
                            settableEnd = true;
                        }
                    }
                });

                grid.add(label, i, j);
                grid.setHalignment(label, HPos.CENTER);
            }
        }
    }
}