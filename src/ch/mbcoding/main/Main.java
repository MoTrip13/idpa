package ch.mbcoding.main;

import ch.mbcoding.controller.MainGuiController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;



/**
 * @author Moritz Bolliger
 * @version 26.09.2019 10:57
 * @project Package ch.mbcoding.main von Projekt Pathfinder
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            // View
            FXMLLoader myloader = new FXMLLoader(getClass().getResource("../gui/MainGui.fxml"));
            VBox root = myloader.load();

            //Controller
            MainGuiController controller = (MainGuiController) myloader.getController();

            Scene scene = new Scene(root);
            primaryStage.setTitle("Pathfinder");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void close(Stage primaryStage){
        primaryStage.close();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
